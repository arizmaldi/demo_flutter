import 'package:demo_flutter/common/config/injector.dart';
import 'package:demo_flutter/common/router/routes.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:demo_flutter/presentation/dashboard/dashboard_page.dart';
import 'package:demo_flutter/presentation/favorite_movie_details/bloc/favorite_movie_details_bloc.dart';
import 'package:demo_flutter/presentation/favorite_movie_details/favorite_movie_details.dart';
import 'package:demo_flutter/presentation/favorite_movie_list/bloc/favorite_movie_list_bloc.dart';
import 'package:demo_flutter/presentation/favorite_movie_list/favorite_movie_list_page.dart';
import 'package:demo_flutter/presentation/home/home_page.dart';
import 'package:demo_flutter/presentation/login/login_page.dart';
import 'package:demo_flutter/presentation/notFound/not_found_page.dart';
import 'package:demo_flutter/presentation/search/bloc/omdb_movie_bloc.dart';
import 'package:demo_flutter/presentation/search/search_page.dart';
import 'package:demo_flutter/presentation/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class Router {
  static FavoriteMovieEntity movies;
  static Map<String, WidgetBuilder> routes = {
    Routes.splash: (BuildContext context) => MySplashScreen(),
    Routes.login: (BuildContext context) => MyLoginPage(),
    Routes.home: (BuildContext context) => MyHomePage(
          id: null,
        ),
    Routes.dashboard: (BuildContext context) => MyHomePage(
          page: HomePageOptions.dashboard,
          id: null,
        ),
    Routes.search: (BuildContext context) => MyHomePage(
          page: HomePageOptions.search,
          id: null,
        ),
    Routes.favoriteList: (BuildContext context) => MyHomePage(
          page: HomePageOptions.favorite,
          id: null,
        ),
    Routes.settings: (BuildContext context) => MyHomePage(
          page: HomePageOptions.setting,
          id: null,
        ),
    Routes.favoriteDetails: (BuildContext context) =>
        BlocProvider<FavoriteMovieDetailsBloc>(
          create: (context) => getIt<FavoriteMovieDetailsBloc>(),
          child: MyMovieDetailsPage(
            favMovie: movies,
          ),
        )
  };

  // static Widget _buildHomePage({HomePageOptions page}) {
  //   return MultiBlocProvider(
  //     providers: [
  //       BlocProvider<OmdbMovieBloc>(
  //         create: (BuildContext context) => getIt<OmdbMovieBloc>(),
  //       ),
  //       BlocProvider<FavoriteMovieListBloc>(
  //         create: (BuildContext context) => getIt<FavoriteMovieListBloc>(),
  //       ),
  //     ],
  //     child: MyHomePage(
  //       page: page,
  //       id: null,
  //     ),
  //   );
  // }

  // static Route _buildSearchRoute => BlocProvider<OmdbMovieSearchBloc>(
  //         create: (context) => getIt<OmdbMovieSearchBloc>(),
  //         child: MySearchPage(
  //         ),
  //       );

  static Route _buildFavoriteDetailsRoute(
    RouteSettings settings, {
    FavoriteMovieEntity favMovie,
  }) {
    var initialEvent;
    if (favMovie != null) {
      initialEvent = FavoriteMoviesDetailsSetEvent(favMovie);
    }
    return MaterialPageRoute(
      builder: (context) => BlocProvider(
          create: (BuildContext context) =>
              getIt<FavoriteMovieDetailsBloc>()..add(initialEvent),
          child: MyFavoriteMovieListPage()),
    );
  }

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final List<String> pathElements = settings.name.split('/');
    if (pathElements[0] != '') {
      return null;
    } else if (pathElements[1] == Routes.favoriteDetailsPath) {
      return MaterialPageRoute(
        builder: (context) {
          return MyDashboardPage();
        },
      );
    }
    return null;
  }

  static Route<dynamic> onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) {
        return MyNotFoundPage();
      },
    );
  }
}
