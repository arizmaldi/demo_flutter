// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:demo_flutter/common/network/movie_client.dart';
import 'package:http/src/client.dart';
import 'package:demo_flutter/common/network/network_check.dart';
import 'package:connectivity/connectivity.dart';
import 'package:demo_flutter/data/favorite_movie/datasources/favorite_movie_remote_datasource.dart';
import 'package:demo_flutter/data/favorite_movie/repositories/favoriteMovie_repositories.dart';
import 'package:demo_flutter/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_flutter/data/omdb_movie/repositories/omdb_repositories.dart';
import 'package:demo_flutter/domain/favorite_movie/repository/favorite_movie_repositories.dart';
import 'package:demo_flutter/domain/favorite_movie/usecases/favorite_movie_usecase.dart';
import 'package:demo_flutter/domain/omdb_movie/repository/omdb_movie_repository.dart';
import 'package:demo_flutter/domain/omdb_movie/usecases/omdb_movie_usecase.dart';
import 'package:demo_flutter/presentation/dashboard/bloc/recomend_movie_bloc.dart';
import 'package:demo_flutter/presentation/favorite_movie_details/bloc/favorite_movie_details_bloc.dart';
import 'package:demo_flutter/presentation/favorite_movie_list/bloc/favorite_movie_list_bloc.dart';
import 'package:demo_flutter/presentation/search/bloc/omdb_movie_bloc.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<MovieClient>(() => MovieClientImpl(getIt<Client>()))
    ..registerLazySingleton<MovieClientImpl>(
        () => MovieClientImpl(getIt<Client>()))
    ..registerFactory<NetworkCheck>(
        () => NetworkCheckImpl(connectivity: getIt<Connectivity>()))
    ..registerLazySingleton<NetworkCheckImpl>(
        () => NetworkCheckImpl(connectivity: getIt<Connectivity>()))
    ..registerFactory<FavoriteMovieRemoteDataSource>(
        () => FavoriteMovieRemoteDataSourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<FavoriteMovieRemoteDataSourceImpl>(
        () => FavoriteMovieRemoteDataSourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<FavoriteMovieRepositoryImpl>(() =>
        FavoriteMovieRepositoryImpl(
            favoriteFavoriteMovieRemoteDatasource:
                getIt<FavoriteMovieRemoteDataSource>()))
    ..registerFactory<OmdbRemoteDataSource>(
        () => OmdbMovieRemoteDataSourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<OmdbMovieRemoteDataSourceImpl>(
        () => OmdbMovieRemoteDataSourceImpl(client: getIt<MovieClient>()))
    ..registerLazySingleton<OmdbMovieRepositoryImpl>(() => OmdbMovieRepositoryImpl(
        omdbMovieNetworkDatasource: getIt<OmdbRemoteDataSource>(),
        networkCheck: getIt<NetworkCheck>()))
    ..registerFactory<FavoriteMovieRepository>(
        () => FavoriteMovieRepositoryImpl(favoriteFavoriteMovieRemoteDatasource: getIt<FavoriteMovieRemoteDataSource>()))
    ..registerLazySingleton<GetAllFavoriteMoviesUsecase>(() => GetAllFavoriteMoviesUsecase(favoriteMovieRepository: getIt<FavoriteMovieRepository>()))
    ..registerLazySingleton<CreateFavoriteMovieUsecase>(() => CreateFavoriteMovieUsecase(favoriteMovieRepository: getIt<FavoriteMovieRepository>()))
    ..registerLazySingleton<UpdateFavoriteMovieUsecase>(() => UpdateFavoriteMovieUsecase(favoriteMovieRepository: getIt<FavoriteMovieRepository>()))
    ..registerLazySingleton<DeleteFavoriteMovieUsecase>(() => DeleteFavoriteMovieUsecase(favoriteMovieRepository: getIt<FavoriteMovieRepository>()))
    ..registerFactory<OmdbMovieRepository>(() => OmdbMovieRepositoryImpl(omdbMovieNetworkDatasource: getIt<OmdbRemoteDataSource>(), networkCheck: getIt<NetworkCheck>()))
    ..registerLazySingleton<GetOmdbMovieUsecase>(() => GetOmdbMovieUsecase(omdbRepository: getIt<OmdbMovieRepository>()))
    ..registerFactory<RecomendMovieBloc>(() => RecomendMovieBloc(getRecomendMovieUsecase: getIt<GetOmdbMovieUsecase>()))
    ..registerFactory<FavoriteMovieDetailsBloc>(() => FavoriteMovieDetailsBloc(updateFavoriteMovieUsecase: getIt<UpdateFavoriteMovieUsecase>()))
    ..registerFactory<FavoriteMovieListBloc>(() => FavoriteMovieListBloc(getAllFavoriteMoviesUsecase: getIt<GetAllFavoriteMoviesUsecase>(), deleteFavoriteMovieUsecase: getIt<DeleteFavoriteMovieUsecase>()))
    ..registerFactory<OmdbMovieSearchBloc>(() => OmdbMovieSearchBloc(getOmdbMovieUsecase: getIt<GetOmdbMovieUsecase>()));
}
