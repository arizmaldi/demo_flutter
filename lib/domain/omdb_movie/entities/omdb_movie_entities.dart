import 'package:meta/meta.dart';

class OmdbMovieEntity {
  final String id;
  final String title;
  final String year;
  final String poster;
  final String label;

  OmdbMovieEntity({
    this.id,
    this.poster,
    @required this.title,
    @required this.year,
    this.label,
  });
}

extension OmdbMovieEntityExt on OmdbMovieEntity {
  OmdbMovieEntity copyWith({
    String id,
    String title,
    String year,
    String poster,
    String label,
  }) =>
      OmdbMovieEntity(
        id: id ?? this.id,
        title: title ?? this.title,
        year: year ?? this.year,
        poster: poster ?? this.poster,
        label: label ?? this.label,
      );
}
