import 'package:demo_flutter/common/models/use_case.dart';
import 'package:demo_flutter/domain/omdb_movie/entities/omdb_movie_entities.dart';
import 'package:demo_flutter/domain/omdb_movie/repository/omdb_movie_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetOmdbMovieUsecase implements UseCase<List<OmdbMovieEntity>, Map> {
  final OmdbMovieRepository omdbRepository;

  GetOmdbMovieUsecase({@required this.omdbRepository});

  @override
  Future<List<OmdbMovieEntity>> call(Map payload) {
    return omdbRepository.getOmdbMovieBySearch(
        payload['title'], payload['year']);
  }
}
