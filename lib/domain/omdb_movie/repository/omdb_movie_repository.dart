import 'package:demo_flutter/data/omdb_movie/repositories/omdb_repositories.dart';
import 'package:demo_flutter/domain/omdb_movie/entities/omdb_movie_entities.dart';
import 'package:injectable/injectable.dart';

@Bind.toType(OmdbMovieRepositoryImpl)
@injectable
abstract class OmdbMovieRepository {
  Future<List<OmdbMovieEntity>> getOmdbMovieBySearch(String title, String year);
}
