import 'package:demo_flutter/common/models/use_case.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:demo_flutter/domain/favorite_movie/repository/favorite_movie_repositories.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetAllFavoriteMoviesUsecase
    implements UseCase<List<FavoriteMovieEntity>, NoPayload> {
  final FavoriteMovieRepository favoriteMovieRepository;

  GetAllFavoriteMoviesUsecase({@required this.favoriteMovieRepository});

  @override
  Future<List<FavoriteMovieEntity>> call(NoPayload _) {
    return favoriteMovieRepository.getAllFavoriteMovie();
  }
}

@lazySingleton
@injectable
class CreateFavoriteMovieUsecase
    implements UseCase<void, FavoriteMovieEntity> {
  final FavoriteMovieRepository favoriteMovieRepository;

  CreateFavoriteMovieUsecase({@required this.favoriteMovieRepository});

  @override
  Future<void> call(FavoriteMovieEntity movie) async {
    await favoriteMovieRepository.postFavoriteMovie(movie);
  }
}

@lazySingleton
@injectable
class UpdateFavoriteMovieUsecase
    implements UseCase<void, FavoriteMovieEntity> {
  final FavoriteMovieRepository favoriteMovieRepository;

  UpdateFavoriteMovieUsecase({@required this.favoriteMovieRepository});

  @override
  Future<void> call(FavoriteMovieEntity movie) {
    return favoriteMovieRepository.updateFavoriteMovie(movie);
  }
}

@lazySingleton
@injectable
class DeleteFavoriteMovieUsecase
    implements UseCase<void, FavoriteMovieEntity> {
  final FavoriteMovieRepository favoriteMovieRepository;

  DeleteFavoriteMovieUsecase({@required this.favoriteMovieRepository});

  @override
  Future<void> call(FavoriteMovieEntity movie) {
    return favoriteMovieRepository.deleteFavoriteMovie(movie);
  }
}
