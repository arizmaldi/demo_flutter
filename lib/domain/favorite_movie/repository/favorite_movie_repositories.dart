import 'package:demo_flutter/data/favorite_movie/repositories/favoriteMovie_repositories.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:injectable/injectable.dart';

@Bind.toType(FavoriteMovieRepositoryImpl)
@injectable
abstract class FavoriteMovieRepository {
  Future<List<FavoriteMovieEntity>> getAllFavoriteMovie();
  Future<void> postFavoriteMovie(FavoriteMovieEntity movie);
  Future<void> updateFavoriteMovie(FavoriteMovieEntity movie);
  Future<void> deleteFavoriteMovie(FavoriteMovieEntity movie);
}
