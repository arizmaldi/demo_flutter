import 'package:meta/meta.dart';

class FavoriteMovieEntity {
  final String id;
  final String title;
  final String year;
  final String poster;
  final String label;
  final String type;
  final int priority;
  final bool viewed;
  final int rating;
  final int timestamp;

  FavoriteMovieEntity(
      {this.id,
      this.poster,
      @required this.title,
      @required this.year,
      this.label,
      this.priority,
      this.viewed,
      this.rating,
      this.timestamp,
      this.type});
}

extension FavoriteMovieEntityExt on FavoriteMovieEntity {
  FavoriteMovieEntity copyWith({
    String id,
    String title,
    String year,
    String poster,
    String label,
    String type,
    int priority,
    bool viewed,
    int rating,
    int timestamp,
  }) =>
      FavoriteMovieEntity(
          id: id ?? this.id,
          title: title ?? this.title,
          year: year ?? this.year,
          poster: poster ?? this.poster,
          label: label ?? this.label,
          type: type ?? this.type,
          priority: priority ?? this.priority,
          viewed: viewed ?? this.viewed,
          rating: rating ?? this.rating,
          timestamp: timestamp ?? this.timestamp);
}
