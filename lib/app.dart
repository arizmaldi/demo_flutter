import 'package:demo_flutter/presentation/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'common/router/router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: Router.routes,
      onGenerateRoute: Router.onGenerateRoute,
      onUnknownRoute: Router.onUnknownRoute,
      theme: ThemeData(
          accentColor: Colors.black,
          primaryColor: Colors.black,
          hintColor: Colors.red),
      home: MySplashScreen(),
    );
  }
}
