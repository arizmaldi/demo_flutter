import 'package:demo_flutter/common/config/injector.dart';
import 'package:demo_flutter/data/favorite_movie/datasources/favorite_movie_remote_datasource.dart';
import 'package:demo_flutter/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_flutter/data/omdb_movie/models/omdb_movie.dart';
import 'package:demo_flutter/presentation/search/bloc/omdb_movie_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MySearchPage extends StatefulWidget {
  MySearchPage({Key key}) : super(key: key);

  @override
  _MySearchPageState createState() => _MySearchPageState();
}

class _MySearchPageState extends State<MySearchPage> {
  List<MovieOmdb> _movieList;
  var items = List<MovieOmdb>();
  String _keyword;
  String _year;

  @override
  void initState() {
    _keyword = 'hello';
    _year = '';
    // BlocProvider.of<OmdbMovieSearchBloc>(context)
    //     .add(OmdbMovieLoadEvent());
    _getData();
    super.initState();
  }

  final OmdbRemoteDataSource omdbRDS = getIt<OmdbRemoteDataSource>();

  final FavoriteMovieRemoteDataSource favMovieRDS =
      getIt<FavoriteMovieRemoteDataSource>();

  void _getData() async {
    try {
      final list = await omdbRDS.searchMovie(_keyword, _year);
      if (mounted) {
        setState(() {
          _movieList = list;
        });
      }
    } catch (e) {
      print(e);
      if (mounted) {
        setState(() {
          _movieList = null;
        });
      }
    }
  }

  void filterSearchResultsByTitle(String title) {
    setState(() {
      _keyword = title;
    });
  }

  void filterSearchResultsByYear(String year) {
    setState(() {
      _year = year;
    });
  }

  Widget _buildMovieList() {
    if (_movieList == null) {
      return _buildProgressBar();
    } else {
      return GridView.builder(
        itemCount: _movieList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 1.34),
        ),
        itemBuilder: _buildItemList,
      );
    }
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildItemList(BuildContext context, int index) {
    final movie = _movieList[index];
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  movie.poster != "N/A"
                      ? movie.poster
                      : "https://mediacenter.surabaya.go.id/fotos/no-image.png",
                  fit: BoxFit.fill,
                  height: 185,
                  width: 187,
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.only(left: 8, right: 35),
                title: Text(
                  movie.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text(movie.year),
              ),
            ],
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          final snackBar = SnackBar(
              content: Text(
            '${movie.title} Added to the favorite movie',
            textAlign: TextAlign.center,
          ));
          favMovieRDS.postFavoriteMovie(
              movie.id, movie.title, movie.year, movie.poster, 'No Label');
          Scaffold.of(context).showSnackBar(snackBar);
        },
        mini: true,
        child: Icon(Icons.favorite_border),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Widget build(BuildContext context) {
    return new Container(
        height: 500.0,
        child: new GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Column(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(
                      top: 8, bottom: 5, left: 10, right: 10),
                  child: new Row(
                    children: <Widget>[
                      new Flexible(
                        flex: 6,
                        child: TextField(
                          style: TextStyle(height: 1, fontSize: 15),
                          onChanged: (value) {
                            filterSearchResultsByTitle(
                                value != "" ? value : "Tom and jerry");
                            // BlocProvider.of<OmdbMovieSearchBloc>(context).add(
                            //     (OmdbMovieLoadByTitleEvent(value)));
                          },
                          decoration: InputDecoration(
                              labelStyle: TextStyle(color: Colors.grey),
                              contentPadding:
                                  const EdgeInsets.symmetric(vertical: 15),
                              labelText: "Search",
                              prefixIcon: Icon(Icons.search),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25.0)))),
                        ),
                      ),
                      Spacer(),
                      new Flexible(
                        flex: 3,
                        child: TextField(
                          keyboardType: TextInputType.number,
                          style: TextStyle(height: 1, fontSize: 15),
                          onChanged: (value) {
                            filterSearchResultsByYear(value);
                            // BlocProvider.of<OmdbMovieSearchBloc>(context)
                            //     .add((OmdbMovieLoadByYearEvent(value)));
                          },
                          decoration: InputDecoration(
                              labelStyle: TextStyle(color: Colors.grey),
                              contentPadding:
                                  const EdgeInsets.symmetric(vertical: 15),
                              labelText: "Year",
                              prefixIcon: Icon(Icons.date_range),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25.0)))),
                        ),
                      ),
                    ],
                  )),
              Expanded(
                child: FutureBuilder(
                  future: omdbRDS.searchMovie(_keyword, _year),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<MovieOmdb>> snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        !snapshot.hasError) {
                      _movieList = snapshot.data;
                      return _buildMovieList();
                    } else {
                      return _buildProgressBar();
                    }
                  },
                ),
              )
            ],
          ),
        ));
  }
}
