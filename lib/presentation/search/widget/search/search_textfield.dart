// import 'package:demo_flutter/common/network/movie_client.dart';
// import 'package:demo_flutter/data/omdb_movie/datasources/omdb_local_datasource.dart';
// import 'package:demo_flutter/data/omdb_movie/models/omdb_movie.dart';
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;

// class SearchTextField extends StatefulWidget {
//   SearchTextField({Key key}) : super(key: key);

//   @override
//   _SearchTextFieldState createState() => _SearchTextFieldState();
// }

// class _SearchTextFieldState extends State<SearchTextField> {
//   var items = List<MovieOmdb>();

//   OmdbRemoteDataSource get omdbRDS => OmdbMovieRemoteDataSourceImpl(
//         client: MovieClientImpl(http.Client()),
//       );

//   Widget build(BuildContext context) {
//     return new Column(
//       children: <Widget>[
//         Padding(
//             padding:
//                 const EdgeInsets.only(top: 8, bottom: 5, left: 10, right: 10),
//             child: new Row(
//               children: <Widget>[
//                 new Flexible(
//                   flex: 6,
//                   child: TextField(
//                     style: TextStyle(height: 1, fontSize: 15),
//                     onChanged: (value) {
//                       filterSearchResultsByTitle(
//                           value != "" ? value : "Tom and jerry");
//                     },
//                     decoration: InputDecoration(
//                         contentPadding:
//                             const EdgeInsets.symmetric(vertical: 15),
//                         labelText: "Search",
//                         prefixIcon: Icon(Icons.search),
//                         border: OutlineInputBorder(
//                             borderRadius:
//                                 BorderRadius.all(Radius.circular(25.0)))),
//                   ),
//                 ),
//                 Spacer(),
//                 new Flexible(
//                   flex: 3,
//                   child: TextField(
//                     keyboardType: TextInputType.number,
//                     style: TextStyle(height: 1, fontSize: 15),
//                     onChanged: (value) {
//                       filterSearchResultsByYear(value);
//                     },
//                     decoration: InputDecoration(
//                         contentPadding:
//                             const EdgeInsets.symmetric(vertical: 15),
//                         labelText: "Year",
//                         prefixIcon: Icon(Icons.date_range),
//                         border: OutlineInputBorder(
//                             borderRadius:
//                                 BorderRadius.all(Radius.circular(25.0)))),
//                   ),
//                 ),
//               ],
//             )),
//       ],
//     );
//   }
// }
