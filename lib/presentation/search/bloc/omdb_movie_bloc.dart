import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_flutter/domain/omdb_movie/entities/omdb_movie_entities.dart';
import 'package:demo_flutter/domain/omdb_movie/usecases/omdb_movie_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'omdb_movie_event.dart';
part 'omdb_movie_state.dart';

@injectable
class OmdbMovieSearchBloc extends Bloc<OmdbMovieEvent, OmdbMovieState> {
  final GetOmdbMovieUsecase getOmdbMovieUsecase;

  OmdbMovieSearchBloc({@required this.getOmdbMovieUsecase});

  @override
  OmdbMovieState get initialState => const OmdbMovieInitialState();

  @override
  Stream<OmdbMovieState> mapEventToState(
    OmdbMovieEvent event,
  ) async* {
    if (event is OmdbMovieLoadEvent) {
      final String title = "";
      final String year = "";

      yield* _loadOmdbMovieBySearch(title, year);
    } else if (event is OmdbMovieLoadBySearchEvent) {
      yield* _loadOmdbMovieBySearch(event.title, event.year);
    }
  }

  Stream<OmdbMovieState> _loadOmdbMovieBySearch(
      String title, String year) async* {
    yield OmdbMovieLoadingState(title: title, year: year);
    try {
      final omdbMovieList = await getOmdbMovieUsecase({title:title, year:year});
      yield OmdbMovieDataState(omdbMovieList: omdbMovieList);
    } catch (e) {
      yield OmdbMovieErrorState(e);
    }
  }
}
