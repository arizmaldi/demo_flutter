part of 'omdb_movie_bloc.dart';

@immutable
abstract class OmdbMovieState {
  const OmdbMovieState();
}

@immutable
class OmdbMovieInitialState extends OmdbMovieState {
  const OmdbMovieInitialState();
}

@immutable
class OmdbMovieLoadingState extends OmdbMovieState with EquatableMixin {
  final String title;
  final String year;

  OmdbMovieLoadingState({this.title, this.year});

  @override
  List<Object> get props => [title, year];
}

@immutable
class OmdbMovieDataState extends OmdbMovieState with EquatableMixin {
  final List<OmdbMovieEntity> omdbMovieList;

  OmdbMovieDataState({this.omdbMovieList});

  @override
  List<Object> get props => [omdbMovieList];
}

@immutable
class OmdbMovieErrorState extends OmdbMovieState with EquatableMixin {
  final dynamic error;

  OmdbMovieErrorState(this.error);

  @override
  List<Object> get props => [error];
}
