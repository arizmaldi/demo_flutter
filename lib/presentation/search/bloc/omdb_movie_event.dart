part of 'omdb_movie_bloc.dart';

@immutable
abstract class OmdbMovieEvent {}

@immutable
class OmdbMovieLoadEvent extends OmdbMovieEvent {}

@immutable
class OmdbMovieLoadBySearchEvent extends OmdbMovieEvent {
  final String title;
  final String year;

  OmdbMovieLoadBySearchEvent(this.title, this.year);
}
