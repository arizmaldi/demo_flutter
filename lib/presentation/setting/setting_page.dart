
import 'package:demo_flutter/common/router/routes.dart';
import 'package:flutter/material.dart';

class MySettingPage extends StatelessWidget {
  MySettingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CheckboxListTile(
          title: Text("Recomendation"),
          value: true,
          onChanged: (newValue) {},
          controlAffinity:
              ListTileControlAffinity.leading, //  <-- leading Checkbox
        ),
      ),
      floatingActionButton: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(30.0),
          color: Colors.black,
          child: MaterialButton(
              minWidth: 200,
              padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(Routes.login);
              },
              child: Text(
                "Logout",
                textAlign: TextAlign.center,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ))),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
