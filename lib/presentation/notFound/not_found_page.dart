import 'package:flutter/material.dart';

class MyNotFoundPage extends StatelessWidget {
  const MyNotFoundPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text('NotFoundPage')),
    );
  }
}
