import 'package:demo_flutter/common/network/movie_client.dart';
import 'package:demo_flutter/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_flutter/data/omdb_movie/models/omdb_movie.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

class MyDashboardPage extends StatefulWidget {
  MyDashboardPage({Key key}) : super(key: key);

  @override
  _MyDashboardPageState createState() => _MyDashboardPageState();
}

class _MyDashboardPageState extends State<MyDashboardPage> {
  TextEditingController editingController = TextEditingController();
  List<MovieOmdb> _movieListMV;
  List<MovieOmdb> _movieListP;
  var items = List<MovieOmdb>();

  @override
  void initState() {
    super.initState();
  }

  OmdbRemoteDataSource get omdbRDS => OmdbMovieRemoteDataSourceImpl(
        client: MovieClientImpl(http.Client()),
      );

  Widget _buildMovieList() {
    if (_movieListMV == null) {
      return _buildProgressBar();
    } else {
      return GridView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _movieListMV.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 3),
        ),
        itemBuilder: _buildItemList,
      );
    }
  }

  Widget _buildMovieList2() {
    if (_movieListP == null) {
      return _buildProgressBar();
    } else {
      return GridView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _movieListP.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 3),
        ),
        itemBuilder: _buildItemListP,
      );
    }
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildItemList(BuildContext context, int index) {
    final movieMV = _movieListMV[index];
    return Scaffold(
      body: Container(
        height: 310,
        padding: const EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  movieMV.poster != "N/A"
                      ? movieMV.poster
                      : "https://mediacenter.surabaya.go.id/fotos/no-image.png",
                  fit: BoxFit.fill,
                  height: 170,
                  width: 170,
                ),
              ),
              ListTile(
                title: Text(
                  movieMV.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text(movieMV.year),
              ),
            ],
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      ),
    );
  }

  Widget _buildItemListP(BuildContext context, int index) {
    final movieP = _movieListP[index];
    return Scaffold(
      body: Container(
        height: 310,
        padding: const EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  movieP.poster != "N/A"
                      ? movieP.poster
                      : "https://mediacenter.surabaya.go.id/fotos/no-image.png",
                  fit: BoxFit.fill,
                  height: 170,
                  width: 170,
                ),
              ),
              ListTile(
                title: Text(
                  movieP.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text(movieP.year),
              ),
            ],
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Align(
            child: Container(
                padding: EdgeInsets.only(left: 10, right: 7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      'Most viewed',
                      style: GoogleFonts.carterOne(fontSize: 24),
                    ),
                    Spacer(),
                    Text(
                      'SEE',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          color: Colors.grey),
                    ),
                  ],
                )),
          ),
          Expanded(
            child: FutureBuilder(
              future: omdbRDS.searchMovie('Despicable', ''),
              builder: (BuildContext context,
                  AsyncSnapshot<List<MovieOmdb>> snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError) {
                  _movieListMV = snapshot.data;
                  return _buildMovieList();
                } else {
                  return _buildProgressBar();
                }
              },
            ),
          ),
          Align(
            child: Container(
                padding: EdgeInsets.only(left: 10, right: 7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text('Most popular',
                        style: GoogleFonts.carterOne(fontSize: 24)),
                    Spacer(),
                    Text(
                      'SEE',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          color: Colors.grey),
                    ),
                  ],
                )),
          ),
          Expanded(
            child: FutureBuilder(
              future: omdbRDS.searchMovie('Harry', ''),
              builder: (BuildContext context,
                  AsyncSnapshot<List<MovieOmdb>> snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError) {
                  _movieListP = snapshot.data;
                  return _buildMovieList2();
                } else {
                  return _buildProgressBar();
                }
              },
            ),
          ),
          SizedBox(
            height: 4,
          )
        ],
      ),
    );
  }
}
