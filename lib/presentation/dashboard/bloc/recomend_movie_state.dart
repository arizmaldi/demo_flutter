part of 'recomend_movie_bloc.dart';

@immutable
abstract class RecomendMovieState {
  const RecomendMovieState();
}

@immutable
class RecomendMovieInitialState extends RecomendMovieState {
  const RecomendMovieInitialState();
}

@immutable
class RecomendMovieLoadingState extends RecomendMovieState with EquatableMixin {
  final String title;
  final String year;

  RecomendMovieLoadingState({this.title, this.year});

  @override
  List<Object> get props => [title, year];
}

@immutable
class RecomendMovieDataState extends RecomendMovieState with EquatableMixin {
  final List<OmdbMovieEntity> omdbMovieList;

  RecomendMovieDataState({this.omdbMovieList});

  @override
  List<Object> get props => [omdbMovieList];
}

@immutable
class RecomendMovieErrorState extends RecomendMovieState with EquatableMixin {
  final dynamic error;

  RecomendMovieErrorState(this.error);

  @override
  List<Object> get props => [error];
}
