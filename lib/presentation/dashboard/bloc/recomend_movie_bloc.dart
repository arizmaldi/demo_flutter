import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_flutter/domain/omdb_movie/entities/omdb_movie_entities.dart';
import 'package:demo_flutter/domain/omdb_movie/usecases/omdb_movie_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';


part 'recomend_movie_event.dart';
part 'recomend_movie_state.dart';

@injectable
class RecomendMovieBloc extends Bloc<RecomendMovieEvent, RecomendMovieState> {
  //Here i use omdb use case
  final GetOmdbMovieUsecase getRecomendMovieUsecase;

  RecomendMovieBloc({@required this.getRecomendMovieUsecase});

  @override
  RecomendMovieState get initialState => const RecomendMovieInitialState();

  @override
  Stream<RecomendMovieState> mapEventToState(
    RecomendMovieEvent event,
  ) async* {
    if (event is RecomendMovieLoadEvent) {
      final String title = "";
      final String year = "";

      yield* _loadRecomendMovieBySearch(title, year);
    } else if (event is RecomendMovieLoadBySearchEvent) {
      yield* _loadRecomendMovieBySearch(event.title, event.year);
    }
  }

  Stream<RecomendMovieState> _loadRecomendMovieBySearch(
      String title, String year) async* {
    yield RecomendMovieLoadingState(title: title, year: year);
    try {
      final omdbMovieList =
          await getRecomendMovieUsecase({title: title, year: year});
      yield RecomendMovieDataState(omdbMovieList: omdbMovieList);
    } catch (e) {
      yield RecomendMovieErrorState(e);
    }
  }
}
