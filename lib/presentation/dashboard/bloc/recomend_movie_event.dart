part of 'recomend_movie_bloc.dart';

@immutable
abstract class RecomendMovieEvent {}

@immutable
class RecomendMovieLoadEvent extends RecomendMovieEvent {}

@immutable
class RecomendMovieLoadBySearchEvent extends RecomendMovieEvent {
  final String title;
  final String year;

  RecomendMovieLoadBySearchEvent(this.title, this.year);
}
