import 'package:demo_flutter/common/router/routes.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

typedef DrawerCallback = Function(int index);

class HomeDrawer extends StatefulWidget {
  final DrawerCallback callback;

  const HomeDrawer({Key key, @required this.callback}) : super(key: key);

  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  String text;

  @override
  void initState() {
    fetchId();
    super.initState();
  }

  Future<void> fetchId() async {
    SharedPreferences _fetch = await SharedPreferences.getInstance();
    setState(() {
      text = _fetch.getString('id');
    });
  }

  void _showDashboardPage() {
    widget.callback(0);
    Navigator.of(context).pop();
  }

  void _showSearchPage() {
    widget.callback(1);
    Navigator.of(context).pop();
  }

  void _showFavoriteListPage() {
    widget.callback(2);
    Navigator.of(context).pop();
  }

  void _showSettingsPage() {
    widget.callback(3);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: SafeArea(
      child: ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.black87),
            currentAccountPicture: Image.asset("assets/images/indosiar.png"),
            accountName: new Text(text),
            accountEmail: new Text("$text@dktalis.com"),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: _showDashboardPage,
          ),
          ListTile(
            leading: Icon(Icons.search),
            title: Text('Search'),
            onTap: _showSearchPage,
          ),
          ListTile(
            leading: Icon(Icons.favorite),
            title: Text('Favorite'),
            onTap: _showFavoriteListPage,
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Setting'),
            onTap: _showSettingsPage,
          ),
          ListTile(
              title: Center(
                child: Text(
                  'Logout',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ),
              onTap: () async {
                SharedPreferences _prefs =
                    await SharedPreferences.getInstance();
                _prefs.remove("id");
                Navigator.of(context).pushReplacementNamed(Routes.login);
              })
        ],
      ),
    ));
  }
}
