import 'dart:io';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:demo_flutter/presentation/dashboard/dashboard_page.dart';
import 'package:demo_flutter/presentation/favorite_movie_list/favorite_movie_list_page.dart';
import 'package:demo_flutter/presentation/search/search_page.dart';
import 'package:demo_flutter/presentation/setting/setting_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'widgets/drawer/home_drawer.dart';

enum HomePageOptions { dashboard, search, favorite, setting }

class MyHomePage extends StatefulWidget {
  final HomePageOptions page;
  final String id;

  const MyHomePage({Key key, this.page, @required this.id}) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHomePage> {
  var _titleBar = 'Home';
  int _selectedPageIndex;

  @override
  void initState() {
    super.initState();
    _selectedPageIndex = widget.page?.index ?? 0;
  }

  final List<Widget> _pages = [
    MyDashboardPage(),
    MySearchPage(),
    MyFavoriteMovieListPage(),
    MySettingPage(),
  ];

  void _onItemSelected(int index) {
    setState(() {
      if (index == 0) {
        _titleBar = 'Home';
      } else if (index == 1) {
        _titleBar = 'Search Movie';
      } else if (index == 2) {
        _titleBar = 'Favorite Movie';
      } else {
        _titleBar = 'Setting';
      }
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text(
          '$_titleBar',
          style: GoogleFonts.carterOne(fontSize: 24),
        ),
        // title: Text('$_titleBar', style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),),
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
            iconSize: 20,
            icon: new Image.asset('assets/images/indosiar.png'),
            onPressed: () => exit(0),
          )
        ],
      ),
      drawer: HomeDrawer(
        callback: _onItemSelected,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: _pages[_selectedPageIndex]),
      bottomNavigationBar: CurvedNavigationBar(
        height: 45,
        color: Colors.black,
        buttonBackgroundColor: Colors.black,
        backgroundColor: Colors.transparent,
        items: const <Widget>[
          Icon(
            Icons.home,
            size: 25,
            color: Colors.white,
          ),
          Icon(
            Icons.search,
            size: 25,
            color: Colors.white,
          ),
          Icon(
            Icons.favorite,
            size: 25,
            color: Colors.white,
          ),
          Icon(
            Icons.settings,
            size: 25,
            color: Colors.white,
          )
        ],
        onTap: _onItemSelected,
      ),
    );
  }
}
