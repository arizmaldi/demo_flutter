import 'package:demo_flutter/presentation/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Define a custom Form widget.
class MyLoginPage extends StatefulWidget {
  @override
  MyLoginState createState() => MyLoginState();
}

// Define a corresponding State class.
// This class holds data related to the form.
class MyLoginState extends State<MyLoginPage> {
  final _formKey = GlobalKey<FormState>();
  TextStyle style = TextStyle(fontSize: 20);

  TextEditingController textFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final idField = TextFormField(
      controller: textFieldController,
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      },
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Id User",
          hintStyle: TextStyle(color: Colors.grey),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    //LOGIN BUTTON
    final loginButon = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.black87,
        child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            onPressed: () async {
              SharedPreferences _prefs = await SharedPreferences.getInstance();
              _prefs.setString("id", textFieldController.text);
              _sendLoginData(context);
            },
            child: Text(
              "Login",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold),
            )));

    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    SizedBox(
                      height: 225.0,
                      child: Image.asset(
                        "assets/images/indosiar.png",
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    const Positioned(
                        left: 25,
                        bottom: 20,
                        child: Text(
                          'Please input your id user',
                          style: TextStyle(fontSize: 16),
                        )),
                  ],
                ),
                SizedBox(height: 20.0),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      idField,
                      SizedBox(
                        height: 10,
                      ),
                      loginButon,
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _sendLoginData(BuildContext context) {
    String id = textFieldController.text;
    if (_formKey.currentState.validate()) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => MyHomePage(id: id)));
    }
  }
}
