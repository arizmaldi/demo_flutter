import 'package:demo_flutter/common/network/movie_client.dart';
import 'package:demo_flutter/data/favorite_movie/datasources/favorite_movie_remote_datasource.dart';
import 'package:demo_flutter/data/favorite_movie/models/favoriteMovie.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:demo_flutter/presentation/favorite_movie_details/bloc/favorite_movie_details_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:toggle_switch/toggle_switch.dart';

class MyMovieDetailsPage extends StatefulWidget {
  final FavoriteMovie favMovie;
  const MyMovieDetailsPage({Key key, @required this.favMovie})
      : super(key: key);

  @override
  _MyMovieDetailsPageState createState() => _MyMovieDetailsPageState(favMovie);
}

class _MyMovieDetailsPageState extends State<MyMovieDetailsPage> {
  final _formKey = GlobalKey<FormState>();
  // FavoriteMovieEntity _favMovie;
  FavoriteMovie _favMovie;

  Future<void> _submitForm(FavoriteMovie movie) async {
    FavoriteMovie favMovie = FavoriteMovie(
        id: _favMovie.id,
        title: _favMovie.title,
        year: _favMovie.year,
        poster: _favMovie.poster,
        label: _favMovie.label,
        priority: _favMovie.priority,
        rating: _favMovie.rating,
        viewed: _favMovie.viewed);
    await favMovieRDS.updateFavoriteMoviesByid(favMovie);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _save() {
    if (_favMovie != null) {
      final updateMovie = _favMovie.copyWith(
          id: _favMovie.id,
          title: _favMovie.title,
          poster: _favMovie.poster,
          year: _favMovie.year,
          label: _favMovie.label,
          priority: _favMovie.priority,
          rating: _favMovie.rating,
          viewed: _favMovie.viewed);
      BlocProvider.of<FavoriteMovieDetailsBloc>(context)
          .add(FavoriteMoviesDetailsSaveEvent(updateMovie));
    }
  }

  _MyMovieDetailsPageState(this._favMovie);

  FavoriteMovieRemoteDataSource get favMovieRDS =>
      FavoriteMovieRemoteDataSourceImpl(
        client: MovieClientImpl(http.Client()),
      );

  @override
  Widget build(BuildContext context) {
    final _topContent = Stack(
      children: <Widget>[
        Container(
            height: MediaQuery.of(context).size.height * 0.45,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(45),
                  bottomRight: Radius.circular(45)),
              image: new DecorationImage(
                image: new NetworkImage(_favMovie.poster),
                fit: BoxFit.cover,
              ),
            )),
        Container(
          height: MediaQuery.of(context).size.height * 0.45,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Color.fromRGBO(58, 66, 86, .9),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(45),
                bottomRight: Radius.circular(45)),
          ),
          child: Center(
              child: Container(
            child: Card(
              child: Image.network(
                _favMovie.poster,
                fit: BoxFit.cover,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
            ),
          )),
        )
      ],
    );

    final _bottomContent = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(25.0),
      child: Center(
          child: Align(
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Text(_favMovie.title,
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: GoogleFonts.barlow(
                    fontSize: 35,
                    color: Colors.white,
                    fontStyle: FontStyle.italic)),
            SizedBox(
              height: 10,
            ),
            Text(_favMovie.year,
                style: GoogleFonts.barlow(
                    fontSize: 25,
                    color: Colors.white,
                    fontStyle: FontStyle.italic)),
            SizedBox(
              height: 25,
            ),
            Row(
              children: <Widget>[
                Spacer(flex: 2),
                Text(
                  'LABELED',
                  style: TextStyle(
                      fontSize: 15.0, color: Colors.white, wordSpacing: 2),
                ),
                Spacer(
                  flex: 8,
                ),
                RichText(
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                      text: 'PRIORITY',
                      style: TextStyle(
                          fontSize: 15.0, color: Colors.white, wordSpacing: 2),
                    )),
                Spacer(
                  flex: 2,
                )
              ],
            ),
            Row(
              children: <Widget>[
                new Flexible(
                    flex: 4,
                    child: SizedBox(
                      child: TextFormField(
                        initialValue: _favMovie.label,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.barlow(
                            fontSize: 17,
                            color: Colors.white,
                            fontStyle: FontStyle.italic),
                        onChanged: (value) {
                          _favMovie = _favMovie.copyWith(label: value);
                          print(_favMovie.label);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        decoration: InputDecoration(hintText: 'No Label'),
                      ),
                    )),
                Spacer(
                  flex: 2,
                ),
                RatingBar(
                  itemSize: 20,
                  initialRating: _favMovie.priority.toDouble(),
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.favorite,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    _favMovie = _favMovie.copyWith(priority: rating.toInt());
                    print(_favMovie.priority);
                  },
                ),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              children: <Widget>[
                Spacer(
                  flex: 2,
                ),
                RichText(
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                      text: 'VIEWED',
                      style: TextStyle(
                          fontSize: 15.0, color: Colors.white, wordSpacing: 2),
                    )),
                Spacer(
                  flex: 7,
                ),
                RichText(
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                      text: 'RATING',
                      style: TextStyle(
                          fontSize: 15.0, color: Colors.white, wordSpacing: 2),
                    )),
                Spacer(
                  flex: 2,
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                ToggleSwitch(
                    initialLabelIndex: _favMovie.viewed ? 0 : 1,
                    minWidth: 70.0,
                    cornerRadius: 20,
                    activeBgColor: Colors.green,
                    activeTextColor: Colors.white,
                    inactiveBgColor: Colors.grey,
                    inactiveTextColor: Colors.white,
                    labels: ['YES', 'NO'],
                    icons: [Icons.check, Icons.close],
                    activeColors: [Colors.green, Colors.red],
                    onToggle: (index) {
                      print('switched to: $index');
                      _favMovie = _favMovie.copyWith(viewed: index == 0);
                      print(_favMovie.viewed);
                    }),
                Spacer(),
                RatingBar(
                  itemSize: 20,
                  initialRating: _favMovie.rating.toDouble(),
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, index) {
                    switch (index) {
                      case 0:
                        return Icon(
                          Icons.sentiment_very_dissatisfied,
                          color: Colors.red,
                        );
                      case 1:
                        return Icon(
                          Icons.sentiment_dissatisfied,
                          color: Colors.redAccent,
                        );
                      case 2:
                        return Icon(
                          Icons.sentiment_neutral,
                          color: Colors.amber,
                        );
                      case 3:
                        return Icon(
                          Icons.sentiment_satisfied,
                          color: Colors.lightGreen,
                        );
                      case 4:
                        return Icon(
                          Icons.sentiment_very_satisfied,
                          color: Colors.green,
                        );
                    }
                  },
                  onRatingUpdate: (rating) {
                    _favMovie = _favMovie.copyWith(rating: rating.toInt());
                    print(_favMovie.rating);
                  },
                )
              ],
            ),
          ],
        ),
      )),
    );

    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.black,
          title: Text(
            'Movie Details',
            style: GoogleFonts.carterOne(fontSize: 24),
          ),
          actions: <Widget>[
            FloatingActionButton(
              onPressed: () {
                final form = _formKey.currentState;
                form.save();
                _save();
                // _submitForm(_favMovie);
                Navigator.pop(context);
              },
              child: Icon(Icons.save),
            ),
          ],
        ),
        body: Form(
          key: _formKey,
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: ListView(children: <Widget>[_topContent, _bottomContent]),
          ),
        ));
  }
}
