part of 'favorite_movie_details_bloc.dart';

@immutable
abstract class FavoriteMoviesDetailsEvent {}

@immutable
class FavoriteMoviesDetailsSaveEvent extends FavoriteMoviesDetailsEvent {
  final FavoriteMovieEntity favoriteMoviesEntity;

  FavoriteMoviesDetailsSaveEvent(this.favoriteMoviesEntity);
}

@immutable
class FavoriteMoviesDetailsSetEvent extends FavoriteMoviesDetailsEvent {
  final FavoriteMovieEntity favoriteMoviesEntity;

  FavoriteMoviesDetailsSetEvent(this.favoriteMoviesEntity);
}

// @immutable
// class FavoriteMoviesDetailsGetByIdEvent extends FavoriteMoviesDetailsEvent {
//   final int id;

//   FavoriteMoviesDetailsGetByIdEvent(this.id);
// }
