part of 'favorite_movie_details_bloc.dart';

@immutable
abstract class FavoriteMoviesDetailsState {}

class FavoriteMoviesDetailsInitialState extends FavoriteMoviesDetailsState {
}

class FavoriteMoviesDetailLoadingState extends FavoriteMoviesDetailsState {}

class FavoriteMoviesDetailEditState extends FavoriteMoviesDetailsState {
  final FavoriteMovieEntity favoriteMoviesEntity;

  FavoriteMoviesDetailEditState(this.favoriteMoviesEntity);
}

class FavoriteMoviesDetailErrorState extends FavoriteMoviesDetailsState {
  final dynamic error;

  FavoriteMoviesDetailErrorState(this.error);
}

class FavoriteMoviesDetailSavedState extends FavoriteMoviesDetailsState {}
