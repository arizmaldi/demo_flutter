import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';

import 'package:demo_flutter/domain/favorite_movie/usecases/favorite_movie_usecase.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

part 'favorite_movie_details_event.dart';
part 'favorite_movie_details_state.dart';

@injectable
class FavoriteMovieDetailsBloc
    extends Bloc<FavoriteMoviesDetailsEvent, FavoriteMoviesDetailsState> {
  // final GetFavoriteMovieByIdUsecase getFavoriteMovieByIdUsecase;
  final UpdateFavoriteMovieUsecase updateFavoriteMovieUsecase;

  FavoriteMovieDetailsBloc({
    // this.getFavoriteMovieByIdUsecase,
    this.updateFavoriteMovieUsecase,
  });

  @override
  FavoriteMoviesDetailsState get initialState =>
      FavoriteMoviesDetailsInitialState();

  @override
  Stream<FavoriteMoviesDetailsState> mapEventToState(
    FavoriteMoviesDetailsEvent event,
  ) async* {
    // if (event is FavoriteMoviesDetailssGetByIdEvent) {
    //   yield FavoriteMoviesDetailsLoadingState();
    //   try {
    //     final asteroid = await getFavoriteAsteroidByIdUsecase(event.id);
    //     yield FavoriteMoviesDetailsEditState(asteroid);
    //   } catch (e) {
    //     yield FavoriteMoviesDetailsErrorState(e);
    //   }
    // } else if (event is FavoriteMoviesDetailsSetEvent) {
    //   yield FavoriteMoviesDetailsEditState(event.favoriteAsteroidEntity);
    // } else if (event is FavoriteMoviesDetailssSaveEvent) {
    //   yield FavoriteMoviesDetailsLoadingState();
    //   try {
    //     await updateFavoriteAsteroidUsecase(event.favoriteAsteroidEntity);
    //     yield FavoriteMoviesDetailsSavedState();
    //   } catch (e) {
    //     yield FavoriteMoviesDetailsErrorState(e);
    //   }
    // }

    if (event is FavoriteMoviesDetailsSaveEvent) {
      yield FavoriteMoviesDetailLoadingState();
      try {
        await updateFavoriteMovieUsecase(event.favoriteMoviesEntity);
        yield FavoriteMoviesDetailSavedState();
      } catch (e) {
        yield FavoriteMoviesDetailErrorState(e);
      }
    }
  }
}
