import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_flutter/common/models/use_case.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:demo_flutter/domain/favorite_movie/usecases/favorite_movie_usecase.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

part 'favorite_movie_list_event.dart';
part 'favorite_movie_list_state.dart';

@injectable
class FavoriteMovieListBloc
    extends Bloc<FavoriteMovieListEvent, FavoriteMovieListState> {
  final GetAllFavoriteMoviesUsecase getAllFavoriteMoviesUsecase;
  final DeleteFavoriteMovieUsecase deleteFavoriteMovieUsecase;

  FavoriteMovieListBloc({
    this.getAllFavoriteMoviesUsecase,
    this.deleteFavoriteMovieUsecase,
  });

  @override
  FavoriteMovieListState get initialState =>
      FavoriteMovieListInitialState();

  @override
  Stream<FavoriteMovieListState> mapEventToState(
    FavoriteMovieListEvent event,
  ) async* {
    if (event is FavoriteMovieListRefreshEvent) {
      yield FavoriteMovieListLoadingState();
      try {
        final favoriteList =
            await getAllFavoriteMoviesUsecase(const NoPayload());
        yield FavoriteMovieListDataState(favoriteMovieList: favoriteList);
      } catch (e) {
        yield FavoriteMovieListErrorState(e);
      }
    } else if (event is FavoriteMovieListDeleteEvent) {
      yield FavoriteMovieListLoadingState();
      try {
        await deleteFavoriteMovieUsecase(
            (event as FavoriteMovieListDeleteEvent).favoriteMovieEntity);
        final favoriteList =
            await getAllFavoriteMoviesUsecase(const NoPayload());
        yield FavoriteMovieListDataState(favoriteMovieList: favoriteList);
      } catch (e) {
        yield FavoriteMovieListErrorState(e);
      }
    }
  }
}
