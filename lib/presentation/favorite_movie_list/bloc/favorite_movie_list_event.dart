part of 'favorite_movie_list_bloc.dart';

@immutable
abstract class FavoriteMovieListEvent {}

@immutable
class FavoriteMovieListRefreshEvent {}

@immutable
class FavoriteMovieListDeleteEvent {
  final FavoriteMovieEntity favoriteMovieEntity;

  FavoriteMovieListDeleteEvent(this.favoriteMovieEntity);
}
