part of 'favorite_movie_list_bloc.dart';

@immutable
abstract class FavoriteMovieListState {}

class FavoriteMovieListInitialState extends FavoriteMovieListState {}

class FavoriteMovieListLoadingState extends FavoriteMovieListState {}

class FavoriteMovieListDataState extends FavoriteMovieListState {
  final List<FavoriteMovieEntity> favoriteMovieList;

  FavoriteMovieListDataState({this.favoriteMovieList});
}

class FavoriteMovieListErrorState extends FavoriteMovieListState {
  final dynamic error;

  FavoriteMovieListErrorState(this.error);
}
