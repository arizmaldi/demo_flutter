import 'package:demo_flutter/common/config/injector.dart';
import 'package:demo_flutter/data/favorite_movie/datasources/favorite_movie_remote_datasource.dart';
import 'package:demo_flutter/data/favorite_movie/models/favoriteMovie.dart';
import 'package:demo_flutter/presentation/favorite_movie_details/favorite_movie_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/favorite_movie_list_bloc.dart';

class MyFavoriteMovieListPage extends StatefulWidget {
  MyFavoriteMovieListPage({Key key}) : super(key: key);

  @override
  _MyFavoriteMovieListPageState createState() =>
      _MyFavoriteMovieListPageState();
}

class _MyFavoriteMovieListPageState extends State<MyFavoriteMovieListPage> {
  List<FavoriteMovie> _favMovieList;
  var items = List<FavoriteMovie>();

  @override
  void initState() {
    // BlocProvider.of<FavoriteMovieListBloc>(context)
    //     .add(FavoriteMovieListEvent());
    _getData();
    super.initState();
  }

  final FavoriteMovieRemoteDataSource movieRDS =
      getIt<FavoriteMovieRemoteDataSource>();

  void _getData() async {
    try {
      final list = await movieRDS.getFavoriteMovies();
      if (mounted) {
        setState(() {
          _favMovieList = list;
        });
      }
    } catch (e) {
      print(e);
      if (mounted) {
        setState(() {
          _favMovieList = null;
        });
      }
    }
  }

  Widget _buildMovieList() {
    if (_favMovieList == null) {
      return _buildProgressBar();
    } else {
      return GridView.builder(
        itemCount: _favMovieList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 1.25),
        ),
        itemBuilder: _buildItemList,
      );
    }
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildItemList(BuildContext context, int index) {
    final favMovie = _favMovieList[index];
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          child: new InkWell(
            onLongPress: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyMovieDetailsPage(
                            favMovie: favMovie,
                          )));
            },
            child: Column(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    favMovie.poster != "N/A"
                        ? favMovie.poster
                        : "https://mediacenter.surabaya.go.id/fotos/no-image.png",
                    fit: BoxFit.fill,
                    height: 205,
                    width: 170,
                  ),
                ),
                ListTile(
                  title: Text(
                    favMovie.title,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  subtitle: Text(favMovie.year),
                ),
              ],
            ),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: favMovie.id,
        onPressed: () {
          movieRDS.deleteFavoriteMovies(favMovie.id);

          // if (favMovie != null) {
          //   final deleteMovie = favMovie.copyWith(id: favMovie.id);
          //   BlocProvider.of<FavoriteMovieListBloc>(context)
          //       .add(FavoriteMovieListDeleteEvent(deleteMovie));
          // }

          _getData();

          final snackBar = SnackBar(
              content: Text(
            '${favMovie.title} Deleted from the favorite movie',
            textAlign: TextAlign.center,
          ));
          Scaffold.of(context).showSnackBar(snackBar);
        },
        mini: true,
        child: Icon(Icons.favorite_border),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
          ),
          Expanded(
            child: FutureBuilder(
              future: movieRDS.getFavoriteMovies(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<FavoriteMovie>> snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError) {
                  _favMovieList = snapshot.data;
                  return _buildMovieList();
                } else {
                  return _buildProgressBar();
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
