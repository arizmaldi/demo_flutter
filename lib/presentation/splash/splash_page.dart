import 'dart:async';

import 'package:demo_flutter/common/router/routes.dart';
import 'package:demo_flutter/presentation/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

class MySplashScreen extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MySplashScreen> {
  bool isAuthenticated;

  @override
  void initState() {
    super.initState();
    authLogin();
  }

  void authLogin() async => Timer(const Duration(seconds: 2), () async {
        SharedPreferences _prefs = await SharedPreferences.getInstance();
        isAuthenticated = _prefs.containsKey('id') ?? false;
        print(isAuthenticated);
        if (isAuthenticated) {
          Navigator.of(context).pushReplacementNamed(Routes.dashboard);
        }
      });

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 5,
      navigateAfterSeconds: new MyLoginPage(),
      title: new Text(
        'Welcome In Indosiar Movie App',
        style: new TextStyle(
            fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.white),
      ),
      image: new Image.asset("assets/images/indosiar.png"),
      backgroundColor: Colors.black,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 150.0,
      loaderColor: Colors.white,
    );
  }
}
