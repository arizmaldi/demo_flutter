import 'package:demo_flutter/data/favorite_movie/datasources/favorite_movie_remote_datasource.dart';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:demo_flutter/domain/favorite_movie/repository/favorite_movie_repositories.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class FavoriteMovieRepositoryImpl implements FavoriteMovieRepository {
  final FavoriteMovieRemoteDataSource favoriteFavoriteMovieRemoteDatasource;

  FavoriteMovieRepositoryImpl({
    @required this.favoriteFavoriteMovieRemoteDatasource,
  });

  @override
  Future<void> postFavoriteMovie(FavoriteMovieEntity movie) async {
    await favoriteFavoriteMovieRemoteDatasource.postFavoriteMovie(
        movie.id, movie.title, movie.year, movie.poster, movie.label);
  }

  @override
  Future<void> deleteFavoriteMovie(FavoriteMovieEntity movie) async {
    await favoriteFavoriteMovieRemoteDatasource.deleteFavoriteMovies(movie.id);
  }

  // @override
  // Future<FavoriteMovieEntity> getFavoriteMovieById(int id) async {
  //   return await favoriteFavoriteMovieRemoteDatasource.getById(id);
  // }

  @override
  Future<List<FavoriteMovieEntity>> getAllFavoriteMovie() async {
    return await favoriteFavoriteMovieRemoteDatasource.getFavoriteMovies();
  }

  @override
  Future<void> updateFavoriteMovie(FavoriteMovieEntity movie) async {
    await favoriteFavoriteMovieRemoteDatasource.updateFavoriteMoviesByid(movie);
  }
}
