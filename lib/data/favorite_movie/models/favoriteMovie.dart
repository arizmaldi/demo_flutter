import 'dart:convert';
import 'package:demo_flutter/domain/favorite_movie/entities/favorite_movie_entity.dart';
import 'package:meta/meta.dart';

class FavoriteMovie extends FavoriteMovieEntity {
  final String id;
  final String title;
  final String year;
  final String poster;
  final String label;
  final bool viewed;
  final int priority;
  final int rating;

  FavoriteMovie(
      {this.id,
      this.poster,
      @required this.title,
      @required this.year,
      this.label,
      this.viewed,
      this.priority,
      this.rating});

  factory FavoriteMovie.fromJson(Map<String, dynamic> json) {
    return FavoriteMovie(
        id: json['id'],
        title: json['title'],
        year: json['year'],
        poster: json['poster'],
        label: json['label'],
        viewed: json['viewed'],
        priority: json['priority'],
        rating: json['rating']);
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'year': year,
      'poster': poster,
      'label': label,
      'viewed': viewed,
      'priority': priority,
      'rating': rating
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

extension FavoriteMovieExt on FavoriteMovie {
  FavoriteMovie copyWith(
          {String id,
          String title,
          String year,
          String poster,
          String label,
          bool viewed,
          int priority,
          int rating}) =>
      FavoriteMovie(
          id: id ?? this.id,
          title: title ?? this.title,
          year: year ?? this.year,
          poster: poster ?? this.poster,
          label: label ?? this.label,
          viewed: viewed ?? this.viewed,
          priority: priority ?? this.priority,
          rating: rating ?? this.rating);
}
