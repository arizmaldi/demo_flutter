import 'dart:convert';
import 'package:demo_flutter/common/network/movie_client.dart';
import 'package:demo_flutter/data/favorite_movie/models/favoriteMovie.dart';
import 'package:meta/meta.dart';
import 'package:injectable/injectable.dart';

import 'package:http/http.dart' as http;

@Bind.toType(FavoriteMovieRemoteDataSourceImpl)
@injectable
abstract class FavoriteMovieRemoteDataSource {
  Future<List<FavoriteMovie>> getFavoriteMovies();

  Future<bool> postFavoriteMovie(
      String id, String title, String year, String poster, String production);

  Future<bool> updateFavoriteMoviesByid(FavoriteMovie movie);

  Future<bool> deleteFavoriteMovies(String id);
}

@lazySingleton
@injectable
class FavoriteMovieRemoteDataSourceImpl
    implements FavoriteMovieRemoteDataSource {
  final MovieClient client;

  FavoriteMovieRemoteDataSourceImpl({@required this.client});

  factory FavoriteMovieRemoteDataSourceImpl.create() {
    return FavoriteMovieRemoteDataSourceImpl(
      client: MovieClientImpl(http.Client()),
    );
  }

  @override
  Future<List<FavoriteMovie>> getFavoriteMovies() async {
    String uri =
        'https://demo-video-ws-chfmsoli4q-ew.a.run.app/video-ws/videos';
    final response = await client.get(uri, headers: {'token': 'ardi'});
    if (response.statusCode == 200) {
      String json = response.body;
      List<FavoriteMovie> favMovie = [];
      List<dynamic> decodedJson = jsonDecode(json) ?? favMovie;
      decodedJson.forEach((item) {
        favMovie.add(FavoriteMovie.fromJson(item));
      });
      return favMovie;
    }
    return null;
  }

  Future<bool> postFavoriteMovie(
      String id, String title, String year, String poster, String label) async {
    String uri =
        'https://demo-video-ws-chfmsoli4q-ew.a.run.app/video-ws/videos/';
    final response = await client.post(uri,
        headers: {'token': 'ardi'},
        body: jsonEncode({
          "id": id,
          "title": title,
          "poster": poster,
          "year": year,
        }));
    if (response.statusCode == 200) {
      print('Post Worked');
      return true;
    }
    return null;
  }

  @override
  Future<bool> updateFavoriteMoviesByid(FavoriteMovie movie) async {
    String uri =
        'https://demo-video-ws-chfmsoli4q-ew.a.run.app/video-ws/videos/${movie.id}';
    var json = jsonEncode(movie.toJson());
    final response = await client.put(
      uri,
      headers: {'token': 'ardi'},
      body: json,
    );
    if (response.statusCode == 200) {
      print('Update Movie id: $movie.id');
      return true;
    }
    return null;
  }

  @override
  Future<bool> deleteFavoriteMovies(String id) async {
    String uri =
        'https://demo-video-ws-chfmsoli4q-ew.a.run.app/video-ws/videos/$id';
    final response = await client.delete(uri, headers: {'token': 'ardi'});
    if (response.statusCode == 200) {
      print('DELETED Movie id: $id');
      return true;
    }
    return null;
  }
}
