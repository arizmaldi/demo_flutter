import 'dart:convert';
import 'package:demo_flutter/domain/omdb_movie/entities/omdb_movie_entities.dart';
import 'package:meta/meta.dart';

class MovieOmdb extends OmdbMovieEntity {
  final String id;
  final String title;
  final String year;
  final String poster;

  MovieOmdb({this.id, this.poster, @required this.title, @required this.year});

  factory MovieOmdb.fromJson(Map<String, dynamic> json) {
    return MovieOmdb(
        id: json['imdbID'],
        title: json['Title'],
        year: json['Year'],
        poster: json['Poster']);
  }
  Map<String, dynamic> toJson() {
    return {'id': id, 'title': title, 'year': year, 'poster': poster};
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

extension MovieOmdbExt on MovieOmdb {
  MovieOmdb copyWith({String id, String title, String year, String poster}) =>
      MovieOmdb(
          id: id ?? this.id,
          title: title ?? this.title,
          year: year ?? this.year,
          poster: poster ?? this.poster);
}
