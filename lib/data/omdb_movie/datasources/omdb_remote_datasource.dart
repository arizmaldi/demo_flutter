import 'dart:convert';
import 'package:demo_flutter/common/network/movie_client.dart';
import 'package:demo_flutter/data/omdb_movie/models/omdb_movie.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

import 'package:http/http.dart' as http;

@Bind.toType(OmdbMovieRemoteDataSourceImpl)
@injectable
abstract class OmdbRemoteDataSource {
  Future<List<MovieOmdb>> searchMovie(String title, String year);
}

@lazySingleton
@injectable
class OmdbMovieRemoteDataSourceImpl implements OmdbRemoteDataSource {
  final MovieClient client;

  OmdbMovieRemoteDataSourceImpl({@required this.client});

  factory OmdbMovieRemoteDataSourceImpl.create() {
    return OmdbMovieRemoteDataSourceImpl(
      client: MovieClientImpl(http.Client()),
    );
  }

  @override
  Future<List<MovieOmdb>> searchMovie(String title, String year) async {
    Uri uri = Uri.https('www.omdbapi.com', '/',
        {'apikey': '490058dd', 's': title, 'y': year, 'type': 'movie'});
    final response = await client.get(uri);
    if (response.statusCode == 200) {
      String json = response.body;
      List<MovieOmdb> movie = [];
      List<dynamic> decodedJson = jsonDecode(json)['Search'] ?? movie;
      decodedJson.forEach((item) {
        movie.add(MovieOmdb.fromJson(item));
      });
      return movie;
    }
    return null;
  }
}
