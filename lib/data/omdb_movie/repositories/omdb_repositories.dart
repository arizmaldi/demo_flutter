import 'package:demo_flutter/common/errors/no_connection_error.dart';
import 'package:demo_flutter/common/network/network_check.dart';
import 'package:demo_flutter/data/omdb_movie/datasources/omdb_remote_datasource.dart';
import 'package:demo_flutter/domain/omdb_movie/entities/omdb_movie_entities.dart';
import 'package:demo_flutter/domain/omdb_movie/repository/omdb_movie_repository.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class OmdbMovieRepositoryImpl implements OmdbMovieRepository {
  //final OmdbMovieLocalDatasource omdbMovieLocalDatasource;
  final OmdbRemoteDataSource omdbMovieNetworkDatasource;
  final NetworkCheck networkCheck;

  OmdbMovieRepositoryImpl({
    //@required this.omdbMovieLocalDatasource,
    @required this.omdbMovieNetworkDatasource,
    @required this.networkCheck,
  });

  // @override
  // Future<List<OmdbMovieEntity>> searchMovie(String title, String year) async {
  //   // final localData =
  //   //     await omdbMovieLocalDatasource.getOmdbMoviebySearch(title, year);
  //   // if (localData != null && localData.isNotEmpty) {
  //   //   return localData;
  //   // }
  //   if (await networkCheck.isOnline()) {
  //     final omdbMovieList =
  //         await omdbMovieNetworkDatasource.searchMovie(title, year);
  //     //omdbMovieLocalDatasource.getOmdbMoviebySearch(title,year, omdbMovieList);
  //     return omdbMovieList;
  //   }
  //   // return [];
  //   throw NoConnectionError();
  // }

  @override
  Future<List<OmdbMovieEntity>> getOmdbMovieBySearch(
      String title, String year) async {
    if (await networkCheck.isOnline()) {
      final omdbMovieList =
          await omdbMovieNetworkDatasource.searchMovie(title, year);
      //omdbMovieLocalDatasource.getOmdbMoviebySearch(title,year, omdbMovieList);
      return omdbMovieList;
    }
    // return [];
    throw NoConnectionError();
  }
}
